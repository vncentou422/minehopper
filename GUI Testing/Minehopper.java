import java.io.*;
import java.util.*;

public class Minehopper{
    //Instance Variables
    //# of mines
    public static int numMines = 45;
    //instantiations
    private static Player player1;
    private static Shoe shoe;
    private static Field field;
    
    private int moveCount;
    private static boolean gameOver;
    private static String command = "";
    private static int moves = 0;
    private int difficulty;
    
    private InputStreamReader isr;
    private BufferedReader in;
    
    //-----------------

    //Default Constructor
    public Minehopper(){
	moveCount = 0;
	gameOver =  false;
	isr = new InputStreamReader( System.in );
	in = new BufferedReader (isr);
	newGame();
    }
    //--------------
    
    //Methods

    public void newGame(){
	String welcome;
	String z;
	String howtoplay;
	String name = "";
	welcome = "WELCOME TO MINEHOPPER\n";
	welcome += "\nPlease enter your name: ";
	System.out.print(welcome);
	try {
	    name = in.readLine();
	}
	catch ( IOException e){}
	
	player1 = new Player(name);
	shoe = new Shoe();
	field = new Field();
	z = "\nPlease select a difficulty:\n";
	z += "\t1: Amatuer Minehopper\n";
	z += "\t2: Average Minehopper\n";
	z += "\t3: Pro Minehopper\n";
	z += "\t4: Master Minehopper\n";
	System.out.print(z);
	try {
	    difficulty = Integer.parseInt(in.readLine());
	}
	catch (IOException e){}
	
	field.fill();
	shoe.scan(player1,field);
	field.fill2(numMines * difficulty);

	howtoplay = "\nWelcome to the ClineField " + name + "\n";
	howtoplay += "\nYou are a Minehopper in a tragic time of war.\n";
	howtoplay += "\nYou will traverse the dangerous No Man's Land in order to deliver supplies to your allies.\n";
	howtoplay += "\nBy typing 'up', 'down', 'left', and 'right' in the command line, you will be able to move across this dangerous landscape.\n";
	howtoplay += "\nHowever, how will you be able to elude the deadly mines?\n";
	howtoplay += "\nNo fear cause you have your Scanner Boots!\n";
	howtoplay += "\nThese scanner boots will tell you how many mines are nearby your current location so be careful to elude these mines.\n";
	howtoplay += "\nNow go save your allies Minehopper!\n";
	System.out.println(howtoplay);
       
    }
    public void playTurn(Player player1,Field field, Shoe shoe){
	moves++;
	System.out.print("Command:");
	try{
	    command = in.readLine();
	}
	catch (IOException e){}
	if (command.equals("up")){
	    player1.up();
	}
	else if (command.equals("down"))
	    player1.down(field.getDimension());
	else if (command.equals("left"))
	    player1.left();
	else if (command.equals("right"))
	    player1.right(field.getDimension());     
	if (field.hasMine(player1.getX(), player1.getY())){
	    gameOver = true;
	    System.out.println("Game Over. Our brave hero has fallen in " + moves + " moves.");
	   
	}
	else if (player1.getCamp()){
	    gameOver = true;
	    System.out.println("You have reached camp! Congratulations you have won in " + moves + " moves!");
	}
	else{
	    shoe.scan(player1, field);
	    System.out.println(field.printStr());
	    
	   
	}
	 
    }   
	
    //---------
    public static void main (String [] args ){
	Minehopper game = new Minehopper();
	shoe.scan(player1, field);
	System.out.println(field.printStr());
	while (!gameOver){
	    game.playTurn(player1,field, shoe);
	    //System.out.println(field.printStr());
	    //System.out.println(field.printnum());
	    
	}
    }
}

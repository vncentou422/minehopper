import java.util.ArrayList;

public class Player extends Field{
    private int x;
    private int y;
    private String name;
    private boolean camp;
    
    public Player(){
	x = 9;
	y = 2;
	camp = false;
	
    }
    public Player(String z){
	this();
	name = z;
    }
    
    public int getX(){
	return x;
    }
    public int getY(){
	return y;
    }
    public boolean getCamp(){
	return camp;
    }
    public void up(){
	if ((x-3) < 0)
	     System.out.println("Reached Edge of Field");
	
	else
	    x--;
	
    }
    public void down(int dimension){
	if ((x+3) > dimension)
	    System.out.println("Reached Edge of Field");
	
	else
	    x++;
	
    }
    public void left(){
	if ((y-3) < 0)
	     System.out.println("Reached Edge of Field");
       
	else
	    y--;
    }
    public void right(int dimension){
	if ((y+3) ==  dimension){
	    System.out.println("Reached Camp");
	    camp = true;
	}
	else
	    y++;
    }
}

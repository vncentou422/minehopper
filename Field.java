public class Field{
    private String[][] visual;
    private int[][] number;
    private String retStrr;
    private String retNum;
    private int dimension; 
    public Field(){
	visual = new String[30][30];
	number = new int[30][30];
	dimension = 30;
	retStrr = "";
	retNum = "";
    }
    public int getDimension(){
	return dimension;
    }
    public void fill(){
	for (int count = 0; count < visual.length; count++){
	    for (int y = 0; y < visual[count].length; y++){
		visual[count][y] = "[ ]";
	    }
	}
	for (int x = 0; x < visual.length; x++){
	    visual[x][0] = "[|]";
	    visual[x][dimension-1] = "[|]";
	}
	for (int x = 0; x < visual[0].length; x++){
	    visual[0][x] = "[|]";
	    visual[dimension-1][x] = "[|]";
	}
    }
    public void fill2(int numOfMines){
	int counter = numOfMines;
	for (int count = 0; count < number.length; count++){
	    for (int y = 0; y < number[count].length; y++){
		number[count][y] = 0;
	    }
	}
	while (counter > 0){
	    int x = (int)(Math.random() * 30);
	    int y = (int)(Math.random() * 30);
	    if (number[x][y] == 0){
		number[x][y] = 1;
		counter--;
	    }
	}
	for (int x = 0; x < visual.length; x++){
	    number[x][0] = 9;
	    number[x][dimension-1] = 9;
	}
	for (int x = 0; x < visual[0].length; x++){
	    number[0][x] = 9;
	    number[dimension-1][x] = 9;
	}
    }
    public boolean hasMine(int x , int y ){
	return (number[x][y] == 1);

    }
    public void change(int z, int y, String x){
	visual[z][y] = x;
    }
    public String[][] getVisual(){
	return visual;
    }
    public int[][] getNumber(){
	return number;
    }
    public String printStr(){
	retStrr = "";
	for (int x = 0; x < visual.length; x++){
	    String retStr = "";
	    
	    for (int c = 0; c < visual[x].length; c++){
	        
		retStr += visual[x][c];
	    }
	    retStrr += retStr + "\n";
	}
	retStrr += "\n" + "-----------------------------------------------------\n";
	return retStrr;
    }
    public String printnum(){
	retNum = "";
	for (int x = 0; x < number.length; x++){
	    String retStr = "";
	    for (int c = 0; c < number[x].length; c++){
	        
		retStr += number[x][c];
	    }
	    retNum += retStr + "\n";
	}
	return retNum;
    }
    
    
}
    



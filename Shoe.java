import java.util.ArrayList;

public class Shoe extends Player{
    public Shoe() {
    }
    public void scan(Player name, Field field){
	field.fill();
	int x = name.getX();
	int y = name.getY();
	
	for (int counter = (x-1); counter < (x+2); counter++){
	    for (int counter2 = (y - 1); counter2 < (y +2); counter2 ++){
		int num = 0;
		for(int z = (counter-1); z < (counter + 2); z++){
		    for (int c = (counter2 - 1); c < (counter2 + 2); c++){
			if (field.hasMine(z,c))
			    num++;
		    }
		}
		if (num == 0)
		    field.change(counter,counter2,"[-]");
		
		else
		    field.change(counter,counter2, "[" + num + "]");
	    }
	}
	
        
	field.change(x,y,"[x]");
    }		    
}
